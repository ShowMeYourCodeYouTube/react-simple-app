import axios, { type AxiosResponse } from 'axios'
import React, { Component } from 'react'
import { Container } from 'reactstrap'
import './App.scss'
import RoutingContent from './routing/routingContent'
import { NavbarComponent } from './common/components/NavbarComponent/NavbarComponent'
import { BrowserRouter } from 'react-router-dom'
import { type User, type UsersResponse } from './model/users'
import { LOGGER } from './common/logging'

export interface AppState {
  users: User[]
  isInitialized: boolean
  isErrorDuringLoadingUsers: boolean
}

class App extends Component<any, AppState> {
  constructor(props: any) {
    super(props)
    this.state = {
      users: [],
      isInitialized: false,
      isErrorDuringLoadingUsers: false,
    }
  }

  componentDidMount(): void {
    if (!this.state.isInitialized) {
      this.fetchedUsers()
        .then((response: AxiosResponse<UsersResponse>) => {
          LOGGER.info('Successfully fetched users!', response)
          this.setState({
            ...this.state,
            isInitialized: true,
            users: response.data.results,
          })
        })
        .catch((error) => {
          LOGGER.error('Cannot load users!', error)
          this.setState({
            ...this.state,
            isInitialized: true,
            isErrorDuringLoadingUsers: true,
          })
        })
    }
  }

  private async fetchedUsers(): Promise<AxiosResponse<UsersResponse>> {
    const USERS_NUMBER = process.env.REACT_APP_USERS_NUMBER as string
    LOGGER.info(`Fetching users (${USERS_NUMBER})`)
    return await axios.get(`https://randomuser.me/api/?results=${USERS_NUMBER}`)
  }

  private renderLoadingComponent(): JSX.Element {
    return <img className={'loading-img'} src={'loading.gif'} alt='Loading...' />
  }

  private renderRoutingContent(): JSX.Element {
    return (
      <RoutingContent
        topUsers={this.state.users.slice(0, 3)}
        isErrorDuringLoadingTopUsers={this.state.isErrorDuringLoadingUsers}
        isErrorDuringLoadingUsers={this.state.isErrorDuringLoadingUsers}
        users={this.state.users}
      />
    )
  }

  render(): React.ReactNode {
    return (
      <BrowserRouter>
        <React.Fragment>
          <NavbarComponent />
          <Container>
            {this.state.isInitialized ? this.renderRoutingContent() : this.renderLoadingComponent()}
          </Container>
        </React.Fragment>
      </BrowserRouter>
    )
  }
}

export default App
