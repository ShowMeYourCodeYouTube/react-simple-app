import React, { Component } from 'react'
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap'
import { Link } from 'react-router-dom'
import { Routing } from '../../../routing/routes'

export interface NavbarComponentState {
  isOpen: boolean
}

export class NavbarComponent extends Component<Record<string, unknown>, NavbarComponentState> {
  constructor(props: Record<string, unknown>) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false,
    }
  }

  render(): React.ReactNode {
    return (
      <Navbar color='light' light expand='md'>
        <NavbarBrand href='/'>ReactJs App</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className='ml-auto' navbar>
            <NavItem>
              <NavLink tag={Link} to={`/${Routing.HOME}`}>
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to={`/${Routing.USER_LIST}`}>
                Users
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    )
  }

  private toggle(): void {
    this.setState({
      ...this.state,
      isOpen: !this.state.isOpen,
    })
  }
}
