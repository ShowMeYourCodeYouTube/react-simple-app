import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import { NavbarComponent } from './NavbarComponent'
import { BrowserRouter } from 'react-router-dom'

describe('NavbarComponent', () => {
  test('it should mount', () => {
    const page = render(
      <BrowserRouter>
        <NavbarComponent />
      </BrowserRouter>,
    )

    const element = page.container.querySelector('nav')

    expect(element).toBeInTheDocument()
  })

  test('it should render nav links', () => {
    const page = render(
      <BrowserRouter>
        <NavbarComponent />
      </BrowserRouter>,
    )

    const elements = page.container.getElementsByClassName('nav-link')

    expect(elements.length).toBe(2)
  })
})
