import React, { Component } from 'react'
import {
  Alert,
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle,
  Col,
  Container,
  Row,
} from 'reactstrap'
import { type User } from '../../model/users'
import './HomePageContainer.scss'

export interface HomePageComponentProps {
  topUsers: User[]
  isErrorDuringLoading: boolean
}

export interface HomePageComponentState {
  activeIndex: number
}

export class HomePageContainer extends Component<HomePageComponentProps, HomePageComponentState> {
  constructor(props: HomePageComponentProps) {
    super(props)
    this.state = {
      activeIndex: 0,
    }
  }

  render(): React.ReactNode {
    return (
      <Container className={'element-m-spacing-v'}>
        <h1 className='txt-alg-center'>Top users</h1>
        <Row>
          {this.props.isErrorDuringLoading ? (
            <Col>
              <Alert color='danger'>Error during loading top users! Please refresh the page.</Alert>
            </Col>
          ) : (
            this.props.topUsers.map((user: User, index: number) => {
              const userName = `${user.name.first} ${user.name.last}`
              return (
                <Col sm={12 / this.props.topUsers.length} key={index}>
                  <Card className='person-card'>
                    <CardImg src={user.picture.large} alt={userName} className='img-person-card' />
                    <CardBody>
                      <CardTitle>{userName}</CardTitle>
                      <CardSubtitle>
                        <b>Email:</b> {user.email}
                      </CardSubtitle>
                      <CardText>
                        <b>Country:</b> {user.location.country} ({user.location.city})
                      </CardText>
                    </CardBody>
                  </Card>
                </Col>
              )
            })
          )}
        </Row>
      </Container>
    )
  }
}
