import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { screen } from '@testing-library/dom'
import { HomePageContainer } from './HomePageContainer'
import { type User } from '../../model/users'

describe('HomePageContainer', () => {
  test('it should mount', async () => {
    render(<HomePageContainer topUsers={[]} isErrorDuringLoading={false} />)

    expect(await screen.findByText('Top users')).toBeVisible()
  })

  test('it should show an error message', async () => {
    render(<HomePageContainer topUsers={[]} isErrorDuringLoading={true} />)

    expect(
      await screen.findByText('Error during loading top users! Please refresh the page.'),
    ).toBeVisible()
  })

  test('it should render top users', async () => {
    const users: User[] = [
      {
        name: {
          title: 'Madame',
          first: 'Pia',
          last: 'Pierre',
        },
        location: {
          street: {
            number: 7261,
            name: 'Avenue des Ternes',
          },
          country: 'France',
          city: 'Fontenais',
          state: 'Luzern',
          postcode: 1072,
        },
        email: 'pia.pierre@example.com',
        picture: {
          large: 'https://randomuser.me/api/portraits/women/72.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/72.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/72.jpg',
        },
      },
    ]
    const page = render(<HomePageContainer topUsers={users} isErrorDuringLoading={false} />)

    const elements = page.container.getElementsByClassName('card-title')

    expect(elements.length).toBe(1)
    expect(elements[0].textContent?.indexOf('Pia Pierre') === 0).toBeTruthy()
  })
})
