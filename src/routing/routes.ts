export namespace Routing {
  export const HOME = ''
  export const USER_LIST = 'users'
  export const NOT_FOUND = '*'
}
