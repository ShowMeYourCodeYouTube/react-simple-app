import { Routes, Route } from 'react-router-dom'
import React from 'react'
import { type User } from '../model/users'
import { UserListPageContainer } from '../pages/UserListPage/UserListPageContainer'
import NotFoundPage from '../pages/NotFoundPage/NotFoundPage'
import { HomePageContainer } from '../pages/HomePage/HomePageContainer'
import { Routing } from './routes'

interface RoutingContentProps {
  topUsers: User[]
  isErrorDuringLoadingTopUsers: boolean
  users: User[]
  isErrorDuringLoadingUsers: boolean
}

const RoutingContent: React.FC<RoutingContentProps> = (props: RoutingContentProps) => (
  <Routes>
    <Route
      path={`/${Routing.HOME}`}
      element={
        <HomePageContainer
          isErrorDuringLoading={props.isErrorDuringLoadingTopUsers}
          topUsers={props.topUsers}
        />
      }
    />
    <Route
      path={`/${Routing.USER_LIST}`}
      element={
        <UserListPageContainer
          isErrorDuringLoading={props.isErrorDuringLoadingUsers}
          users={props.users}
        />
      }
    />
    <Route path={`/${Routing.NOT_FOUND}`} element={<NotFoundPage />} />
  </Routes>
)

export default RoutingContent
