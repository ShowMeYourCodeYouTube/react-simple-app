import React from 'react'
import { act, cleanup, render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import App from './App'
import axios from 'axios'
import { ElementId } from './model/elementId'

jest.mock('axios')
const mockedAxios = axios as jest.Mocked<typeof axios>

afterEach(cleanup)

describe('App', () => {
  test('it should fetch and display users', async () => {
    mockedAxios.get.mockResolvedValue({
      data: {
        results: [
          {
            name: {
              title: 'Madame',
              first: 'Pia',
              last: 'Pierre',
            },
            location: {
              street: {
                number: 7261,
                name: 'Avenue des Ternes',
              },
              city: 'Fontenais',
              state: 'Luzern',
              postcode: 1072,
            },
            email: 'pia.pierre@example.com',
            picture: {
              large: 'https://randomuser.me/api/portraits/women/72.jpg',
              medium: 'https://randomuser.me/api/portraits/med/women/72.jpg',
              thumbnail: 'https://randomuser.me/api/portraits/thumb/women/72.jpg',
            },
          },
          {
            name: {
              title: 'Mrs',
              first: 'Tilde',
              last: 'Andersen',
            },
            location: {
              street: {
                number: 9422,
                name: 'Runddyssen',
              },
              city: 'Branderup J',
              state: 'Syddanmark',
              postcode: 70229,
            },
            email: 'tilde.andersen@example.com',
            picture: {
              large: 'https://randomuser.me/api/portraits/women/56.jpg',
              medium: 'https://randomuser.me/api/portraits/med/women/56.jpg',
              thumbnail: 'https://randomuser.me/api/portraits/thumb/women/56.jpg',
            },
          },
        ],
        info: {
          seed: '69c4aa5b750827a6',
          results: 3,
          page: 1,
          version: '1.4',
        },
      },
    })

    const page = await act(async () => render(<App />))

    const elements = page.container.getElementsByClassName('card')

    expect(elements.length).toBe(2)
  })

  test('it should route to NOT_FOUND page when there is no routing configured', async () => {
    window.history.pushState({}, '', '/redirect-me-somewhere')

    const page = await act(async () => render(<App />))
    const element = page.container.querySelector(`#${ElementId.NOT_FOUND}`)

    expect(element?.textContent).toContain('The page does not exist.')
  })
})
